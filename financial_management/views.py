# Create your views here.
from django.shortcuts import render_to_response
from cooperative_Bank.financial_management.models import account
from cooperative_Bank.financial_management.models import Customer
from cooperative_Bank.financial_management.models import MutualFund
from cooperative_Bank.financial_management.models import IPO
from cooperative_Bank.financial_management.models import FO
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.http import HttpResponse
from django.utils import simplejson

user_name = None
user_id1 = None
tot_bal1= None
mf_bal1= None
po_bal1= None
fo_bal1= None
nw_bal1= None
mfid=None
p=None
counter=None
quantity=None
price=None
total=None
#def print_vars():
#    print user_name
#    print user_id1
#    print tot_bal1
#    print mf_bal1
#    print po_bal1
#    print fo_bal1
#    print nw_bal1

def login_page(request):
    return render_to_response("login.html")

def account_detail(request):
    user_id = request.POST["user_id"]    
    try:        
        global user_name
        global user_id1 
        global tot_bal1
        global mf_bal1
        global po_bal1
        global fo_bal1
        global nw_bal1
        user = account.objects.get(pk=user_id)
        customer = Customer.objects.get(pk=user_id)
        user_name = customer.customer_Name
        user_id1 = user.user_id
        tot_bal1= int(user.tot_bal)
        mf_bal1= int(user.mf_bal)
        po_bal1= int(user.po_bal)
        fo_bal1= int(user.fo_bal)
        nw_bal1 =(tot_bal1)-(mf_bal1+po_bal1+fo_bal1)
        return render_to_response("accountpage.html",{"user_id1":user_id1 ,  "mf_bal1":mf_bal1 , "tot_bal1":tot_bal1 , "po_bal1":po_bal1 ,"fo_bal1": fo_bal1, "user_name":user_name,"nw_bal1":nw_bal1})

    except account.DoesNotExist:
        return HttpResponseRedirect("/cooperative_Bank/")

 
##if view is MF show him MF page ,IPO show ipo.htm, FO fo.html else error
def mf_detail(request):
    global user_name
    global user_id1 
    global tot_bal1
    global mf_bal1
    global po_bal1
    global fo_bal1
    global nw_bal1
    global mfid
    global p
    counter = 0
    mf_list = MutualFund.objects.all()
    return render_to_response('mf.html',{'mf_list':mf_list,"user_name":user_name ,"tot_bal1":tot_bal1 , "mf_bal1":mf_bal1 ,"nw_bal1":nw_bal1,"mfid":mfid,"p":p,"counter":counter})

def ipo_detail(request):
    global user_name
    global user_id1 
    global tot_bal1
    global mf_bal1
    global po_bal1
    global fo_bal1
    global nw_bal1

    ipo_list = IPO.objects.all()
    return render_to_response('ipo.html',{'ipo_list':ipo_list,"user_name":user_name ,"tot_bal1":tot_bal1 , "po_bal1":po_bal1 ,"nw_bal1":nw_bal1,})
  
def fo_detail(request):
    global user_name
    global user_id1 
    global tot_bal1
    global mf_bal1
    global po_bal1
    global fo_bal1
    global nw_bal1

    fo_list = FO.objects.all()
    return render_to_response('fo.html',{'fo_list':fo_list,"user_name":user_name ,"tot_bal1":tot_bal1 , "fo_bal1":fo_bal1 ,"nw_bal1":nw_bal1,})
def thank_detail(request):
    global user_name
    global user_id1 
    global tot_bal1
    global mf_bal1
    global po_bal1
    global fo_bal1
    global nw_bal1
    global mfid
    global quantity
    global price
    global total
    i=0
    counter=0
    user = account.objects.get(pk=user_id1)
    mfid = request.POST["abc1"]
    p = MutualFund.objects.get(pk=mfid)
    mf_list = MutualFund.objects.all()
    fo_list = FO.objects.all()
    for mf in mf_list:
     i=i+1
     mfid = request.POST["abc"+str(i)]
     quantity = request.POST["abcd"+str(i)]
     if quantity!="":
      p = MutualFund.objects.get(pk=mfid)
      price=int(p.mf_cost)
      total=price*int(quantity)
      if int(quantity)<=int(p.mf_Qty):
       if total<=mf_bal1:
        user.mf_bal=int(user.mf_bal)-total
        user.tot_bal=int(user.tot_bal)-total
        user.save()
        p.mf_Qty=int(p.mf_Qty)-int(quantity)
        p.save()
        counter=1
        mf_bal1=user.mf_bal   
        quantity=p.mf_Qty
       
    return render_to_response('Thank.html',{'fo_list':fo_list,"user_name":user_name ,"tot_bal1":tot_bal1 , "fo_bal1":fo_bal1 ,"nw_bal1":nw_bal1,"user_id1":user_id1,"total":total,"counter":counter,"mf_bal1":mf_bal1,"quantity":quantity})

def thank1_detail(request):
    global user_name
    global user_id1 
    global tot_bal1
    global mf_bal1
    global po_bal1
    global fo_bal1
    global nw_bal1
    global mfid
    global quantity
    global price
    global total
    i=0
    counter=0
    user = account.objects.get(pk=user_id1)
    mfid = request.POST["abc1"]
    p = IPO.objects.get(pk=mfid)
    ipo_list = IPO.objects.all()
    fo_list = FO.objects.all()
    for ipo in ipo_list:
     i=i+1
     mfid = request.POST["abc"+str(i)]
     quantity = request.POST["abcd"+str(i)]
     if quantity!="":
      p = IPO.objects.get(pk=mfid)
      price=int(p.ipo_cost)
      total=price*int(quantity)
      if int(quantity)<=int(p.ipo_Qty):
       if total<=po_bal1:
        user.po_bal=int(user.po_bal)-total
        user.tot_bal=int(user.tot_bal)-total
        user.save()
        counter=1
        mf_bal1=user.po_bal   
        p.ipo_Qty=int(p.ipo_Qty)-int(quantity)
        p.save()
        po_bal1=user.po_bal
       
    return render_to_response('Thank1.html',{'fo_list':fo_list,"user_name":user_name ,"tot_bal1":tot_bal1 , "fo_bal1":fo_bal1 ,"nw_bal1":nw_bal1,"user_id1":user_id1,"total":total,"counter":counter,"mf_bal1":mf_bal1})

def thank2_detail(request):
    global user_name
    global user_id1 
    global tot_bal1
    global mf_bal1
    global po_bal1
    global fo_bal1
    global nw_bal1
    global mfid
    global quantity
    global price
    global total
    i=0
    counter=0
    user = account.objects.get(pk=user_id1)
    fo_list = FO.objects.all()
    for fo in fo_list:
     i=i+1
     mfid = request.POST["abc"+str(i)]
     quantity = request.POST["abcd"+str(i)]
     if quantity!="":
      p = FO.objects.get(pk=mfid)
      price=int(p.fo_cost)
      total=price*int(quantity)
      if int(quantity)<=int(p.fo_Qty):
       if total<=mf_bal1:
        user.fo_bal=int(user.fo_bal)-total
        user.tot_bal=int(user.tot_bal)-total
        user.save() 
        counter=1
        fo_bal1=user.fo_bal   
        p.fo_Qty=int(p.fo_Qty)-int(quantity)
        p.save()
       
       
    return render_to_response('Thank2.html',{'fo_list':fo_list,"user_name":user_name ,"tot_bal1":tot_bal1 , "fo_bal1":fo_bal1 ,"nw_bal1":nw_bal1,"user_id1":user_id1,"total":total,"counter":counter,"mf_bal1":mf_bal1})
